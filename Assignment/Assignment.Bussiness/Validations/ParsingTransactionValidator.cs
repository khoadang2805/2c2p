﻿using Assignment.Commons.Constants;
using Assignment.Models;
using FluentValidation;
using System;
using System.Globalization;

namespace Assignment.Bussiness.Validations
{
    public class ParsingTransactionValidator: AbstractValidator<CreateTransaction>
    {
        public ParsingTransactionValidator()
        {
            RuleFor(p => p.TransactionId)
                .NotEmpty()
                .WithMessage("Transaction Id is required.")
                .Length(1, 50)
                .WithMessage((data, message) =>
                {
                    return $"The value {data.TransactionId} as TransactionId is not valid.";
                });

            RuleFor(p => p.Amount)
                .NotEmpty()
                .WithMessage("Amount is required.")
                .Must((data, amount, context) =>
                {
                    return decimal.TryParse(amount, out decimal amountVal);
                })
                .WithMessage((data, message) => 
                {
                    return $"The value {data.Amount} as Amount is not valid.";
                }); 

            RuleFor(p => p.CurrencyCode)
                .NotEmpty()
                .WithMessage("Currency code is required.")
                .Length(1, 3)
                .WithMessage((data, message) =>
                {
                    return $"The value {data.CurrencyCode} as CurrencyCode is not valid.";
                });

            RuleFor(p => p.Date)
                .NotEmpty()
                .WithMessage("Date is required.")
                .Must((data, date, context) =>
                {
                    if(DateTime.TryParseExact(date, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out DateTime dateVal))
                    {
                        return true;
                    }
                    else if(DateTime.TryParseExact(date, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out dateVal))
                    {
                        return true;
                    }

                    return false;
                })
                .WithMessage((data, message) => 
                {
                    return $"The value {data.Date} as Date is not valid.";
                });

            RuleFor(p => p.Status)
                .NotEmpty()
                .WithMessage("Status is required.")
                .Must((data, status, context) =>
                {
                    return Enum.TryParse(status, out TransactionStatus dateVal);
                })
                .WithMessage((data, message) => 
                {
                    return $"The value {data.Status} as Status is not valid";
                });
        }
    }
}
