﻿using Assignment.Models;
using FluentValidation;

namespace Assignment.Bussiness.Validations
{
    public class UploadTransactionValidator: AbstractValidator<ImportTransaction>
    {
        public UploadTransactionValidator()
        {
            RuleFor(p => p.File)
                .NotEmpty()
                .WithMessage("Please upload file.")
                .Must(m => m.Length < 1024 * 1024)
                .WithMessage("File must be less than 1 MB.");

            RuleFor(p => p.File.FileName)
                .Must((data, fileName, cxt) =>
                {
                    return fileName.EndsWith(".csv") || fileName.EndsWith(".xml");
                })
                .WithMessage("Unknown format.");
        }
    }
}
