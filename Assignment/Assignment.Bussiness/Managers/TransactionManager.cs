﻿using Assignment.Bussiness.CsvMappers;
using Assignment.Bussiness.Validations;
using Assignment.Commons;
using Assignment.Commons.Constants;
using Assignment.DataAccess.Sql.Repositories;
using Assignment.Models;
using Assignment.Models.Exceptions;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Xml;
using TinyCsvParser;

namespace Assignment.Bussiness.Managers
{
    public interface ITransactionManager
    {
        Task ImportTransactions(ImportTransaction request);
        Task<PageResult<Transaction>> GetTransactionsByCurrency(
            string currency,
            int pageNumber,
            int pageSize);
        Task<PageResult<Transaction>> GetTransactionsByDate(
            DateTime? from,
            DateTime? to,
            int pageNumber,
            int pageSize);
        Task<PageResult<Transaction>> GetTransactionsByStatus(
            TransactionStatus? status,
            int pageNumber,
            int pageSize);
    }

    internal class TransactionManager : ITransactionManager
    {
        private readonly char Seperator = ',';
        private readonly ILogger<TransactionManager> _logger;
        private readonly ParsingTransactionValidator _validationRules;
        private readonly ITransactionRepository _transactionRepository;
        private readonly CsvParserOptions _csvParserOptions;
        private readonly CsvTransactionMapping _csvMapper;

        public TransactionManager(
            ILogger<TransactionManager> logger,
            ITransactionRepository transactionRepository)
        {
            _logger = logger;
            _transactionRepository = transactionRepository;
            _validationRules = new ParsingTransactionValidator();
            _csvParserOptions = new(true, Seperator);
            _csvMapper = new();
        }

        private List<CreateTransaction> HandleXmlFile(IFormFile file)
        {
            XmlDocument doc = new();
            doc.Load(file.OpenReadStream());

            List<CreateTransaction> createTransactions = new();

            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                CreateTransaction createTransaction = new();

                createTransaction.TransactionId = node.Attributes["id"]?.Value;

                foreach (XmlNode locNode in node)
                {
                    if (locNode.Name.Equals("TransactionDate", StringComparison.InvariantCultureIgnoreCase))
                    {
                        createTransaction.Date = locNode.InnerText.Trim();
                    }
                    else if(locNode.Name.Equals("PaymentDetails", StringComparison.InvariantCultureIgnoreCase))
                    {
                        foreach (XmlNode paymentDetail in locNode)
                        {
                            if(paymentDetail.Name.Equals("Amount", StringComparison.InvariantCultureIgnoreCase))
                            {
                                createTransaction.Amount = paymentDetail.InnerText.Trim();
                            }
                            else if(paymentDetail.Name.Equals("CurrencyCode", StringComparison.InvariantCultureIgnoreCase))
                            {
                                createTransaction.CurrencyCode = paymentDetail.InnerText.Trim();
                            }
                        }
                    }
                    else if(locNode.Name.Equals("Status", StringComparison.InvariantCultureIgnoreCase))
                    {
                        createTransaction.Status = locNode.InnerText.Trim();
                    }
                }

                createTransactions.Add(createTransaction);
            }

            var validationResults = createTransactions
                .Select(s => _validationRules.Validate(s))
                .ToList();

            if (validationResults.Any(a => !a.IsValid))
            {
                var failures = validationResults
                    .Where(q => q.Errors != null)
                    .SelectMany(s => s.Errors)
                    .ToList();

                _logger.LogWarning("Error while validating data with errors: ", JsonSerializer.Serialize(failures));
                throw new ValidationException(failures);
            }

            return createTransactions;
        }

        private List<CreateTransaction> HandleExcelFile(IFormFile file)
        {
            CsvParser<CreateTransaction> csvParser = new(_csvParserOptions, _csvMapper);
            
            var data = csvParser
                .ReadFromStream(file.OpenReadStream(), Encoding.UTF8)
                .ToList();

            if (data.Any(a => !a.IsValid))
            {
                var errors = data
                 .Where(q => !q.IsValid)
                 .Select(s => s.Error)
                 .ToList();

                _logger.LogWarning("Error while parsing file with error {error}", JsonSerializer.Serialize(errors));
                throw new TransactionValidationException("Invalid transaction file.");
            }

            var results = data
                .Select(x => new { Entity = x.Result, ValidationResult = _validationRules.Validate(x.Result) })
                .ToList();

            if (results.Any(a => !a.ValidationResult.IsValid))
            {
                var failures = results
                    .Where(q => q.ValidationResult.Errors != null)
                    .SelectMany(s => s.ValidationResult.Errors)
                    .ToList();

                _logger.LogWarning("Error while validating data with error {error}", JsonSerializer.Serialize(failures));
                throw new ValidationException(failures);
            }

            var clientEntity = results
                .Select(s => s.Entity)
                .ToList();

            return clientEntity;
        }

        public async Task ImportTransactions(ImportTransaction request)
        {
            List<CreateTransaction> data;
            if (request.File.FileName.EndsWith(".csv"))
            {
                data = HandleExcelFile(request.File);
            }
            else
            {
                data = HandleXmlFile(request.File);
            }

            await _transactionRepository.ImportTransactions(data);
        }

        public async Task<PageResult<Transaction>> GetTransactionsByCurrency(
            string currency,
            int pageNumber,
            int pageSize)
        {
            var result = await _transactionRepository.GetTransactionsByCurrency(currency, pageNumber, pageSize);

            return new PageResult<Transaction>
            {
                TotalCount = result.Item2,
                TotalPages = result.Item3,
                PageSize = pageSize,
                PageNumber = pageNumber,
                Items = result.Item1.ToList()
            };
        }

        public async Task<PageResult<Transaction>> GetTransactionsByStatus(
            TransactionStatus? status,
            int pageNumber,
            int pageSize)
        {
            var result = await _transactionRepository.GetTransactionsByStatus(status, pageNumber, pageSize);

            return new PageResult<Transaction>
            {
                TotalCount = result.Item2,
                TotalPages = result.Item3,
                PageSize = pageSize,
                PageNumber = pageNumber,
                Items = result.Item1.ToList()
            };
        }

        public async Task<PageResult<Transaction>> GetTransactionsByDate(
            DateTime? from,
            DateTime? to,
            int pageNumber,
            int pageSize)
        {
            var result = await _transactionRepository.GetTransactionsByDate(from, to, pageNumber, pageSize);

            return new PageResult<Transaction>
            {
                TotalCount = result.Item2,
                TotalPages = result.Item3,
                PageSize = pageSize,
                PageNumber = pageNumber,
                Items = result.Item1.ToList()
            };
        }
    }
}
