﻿using Assignment.Bussiness.Managers;
using Assignment.Bussiness.Validations;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Assignment.Bussiness
{
    public static class DiExtensions
    {
        public static IServiceCollection AddBusinessLogic(
            this IServiceCollection services)
        {
            services.AddScoped<ITransactionManager, TransactionManager>();

            return services;
        }

    }
}
