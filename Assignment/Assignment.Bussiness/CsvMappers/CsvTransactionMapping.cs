﻿using Assignment.Models;
using TinyCsvParser.Mapping;

namespace Assignment.Bussiness.CsvMappers
{
    internal class CsvTransactionMapping: CsvMapping<CreateTransaction>
    {
        public CsvTransactionMapping()
        {
            MapProperty(0, x => x.TransactionId);
            MapProperty(1, x => x.Amount);
            MapProperty(2, x => x.CurrencyCode);
            MapProperty(3, x => x.Date);
            MapProperty(4, x => x.Status);
        }
    }
}
