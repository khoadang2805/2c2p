﻿using Assignment.DataAccess.Sql.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Assignment.DataAccess.Sql.SqlDb
{
    internal class DatabaseProvisioningService : IHostedService
    {
        private readonly ILogger<DatabaseProvisioningService> _logger;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public DatabaseProvisioningService(
            ILogger<DatabaseProvisioningService> logger,
            IServiceScopeFactory serviceScopeFactory)
        {
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                _logger.LogInformation("(DatabaseProvisioningService) Migrating database");

                using var scope = _serviceScopeFactory.CreateScope();
                var context = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
                await context.Database.EnsureCreatedAsync(cancellationToken);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Error migrating sql database, apllication will shutdown.");
                throw new SqlDatabaseMigrationException(e.InnerException);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
