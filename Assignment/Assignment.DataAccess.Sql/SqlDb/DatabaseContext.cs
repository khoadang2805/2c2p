﻿using Microsoft.EntityFrameworkCore;

namespace Assignment.DataAccess.Sql.SqlDb
{
    public class DatabaseContext: DbContext
    {
        public DbSet<Entities.Transaction> Transactions { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            Entities.Transaction.ConfigureEntity(builder);
        }
    }
}
