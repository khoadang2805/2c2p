﻿using Assignment.Commons.Constants;
using Assignment.DataAccess.Sql.Exceptions;
using Assignment.DataAccess.Sql.SqlDb;
using Assignment.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment.DataAccess.Sql.Repositories
{
    public interface ITransactionRepository
    {
        Task<Transaction> CreateTransaction(Transaction transaction);
        Task<List<Transaction>> ImportTransactions(List<CreateTransaction> transactions);
        Task<(ICollection<Transaction>, int, int)> GetTransactionsByCurrency(
            string currencyCode,
            int pageNumber,
            int pageSize);
        Task<(ICollection<Transaction>, int, int)> GetTransactionsByDate(
            DateTime? from,
            DateTime? to,
            int pageNumber,
            int pageSize);
        Task<(ICollection<Transaction>, int, int)> GetTransactionsByStatus(
            TransactionStatus? status,
            int pageNumber,
            int pageSize);
    }

    internal class TransactionRepository: ITransactionRepository
    {
        private readonly DatabaseContext _dbContext;
        private readonly IMapper _mapper;

        public TransactionRepository(DatabaseContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<List<Transaction>> ImportTransactions(List<CreateTransaction> transactions)
        {
            var dbTransactions = _mapper.Map<List<Entities.Transaction>>(transactions);
            foreach(var dbTransaction in dbTransactions)
            {
                var orgininal = await _dbContext.Transactions.FirstOrDefaultAsync(o => o.TransactionId == dbTransaction.TransactionId);
                if (orgininal != default)
                {
                    orgininal.Amount = dbTransaction.Amount;
                    orgininal.CurrencyCode = dbTransaction.CurrencyCode;
                    orgininal.Date = dbTransaction.Date;
                    orgininal.Status = dbTransaction.Status;

                    _dbContext.Transactions.Update(orgininal);
                }
                else
                {
                    _dbContext.Transactions.Add(dbTransaction);
                }

                await _dbContext.SaveChangesAsync();
            }

            return _mapper.Map<List<Transaction>>(dbTransactions);
        }

        public async Task<Transaction> CreateTransaction(Transaction transaction)
        {
            var dbTransaction = _mapper.Map<Entities.Transaction>(transaction);
            if (await _dbContext.Transactions.FirstOrDefaultAsync(o => o.TransactionId == dbTransaction.TransactionId) != null)
                throw new TransactionAlreadyExistsException(dbTransaction.Id);

            _dbContext.Transactions.Add(dbTransaction);
            await _dbContext.SaveChangesAsync();
            return _mapper.Map<Transaction>(dbTransaction);
        }

        public async Task<(ICollection<Transaction>, int, int)> GetTransactionsByCurrency(
            string currencyCode,
            int pageNumber,
            int pageSize)
        {
            var query = _dbContext.Transactions.AsQueryable();

            if(currencyCode != default)
            {
                query = query.Where(q => q.CurrencyCode == currencyCode);
            }

            var totalItems = await query.CountAsync();
            var totalPages = (int)Math.Ceiling((decimal)totalItems / (decimal)pageSize);

            if (pageNumber > 1)
            {
                query = query.Skip((pageNumber - 1) * pageSize);
            }

            var list = await query
                .Take(pageSize)
                .Select(s => _mapper.Map<Transaction>(s))
                .ToListAsync();

            return (list, totalItems, totalPages);
        }

        public async Task<(ICollection<Transaction>, int, int)> GetTransactionsByStatus(
            TransactionStatus? status,
            int pageNumber,
            int pageSize)
        {
            var query = _dbContext.Transactions.AsQueryable();

            if (status != default)
            {
                query = query.Where(q => q.Status == status);
            }

            var totalItems = await query.CountAsync();
            var totalPages = (int)Math.Ceiling((decimal)totalItems / (decimal)pageSize);

            if (pageNumber > 1)
            {
                query = query.Skip((pageNumber - 1) * pageSize);
            }

            var list = await query
                .Take(pageSize)
                .Select(s => _mapper.Map<Transaction>(s))
                .ToListAsync();

            return (list, totalItems, totalPages);
        }

        public async Task<(ICollection<Transaction>, int, int)> GetTransactionsByDate(
            DateTime? from, 
            DateTime? to,
            int pageNumber,
            int pageSize)
        {
            var query = _dbContext.Transactions.AsQueryable();

            if(from != default)
            {
                query = query.Where(q => q.Date >= from);
            }

            if(to != default)
            {
                query = query.Where(q => q.Date <= to);
            }

            var totalItems = await query.CountAsync();
            var totalPages = (int)Math.Ceiling((decimal)totalItems / (decimal)pageSize);

            if (pageNumber > 1)
            {
                query = query.Skip((pageNumber - 1) * pageSize);
            }

            var list = await query
                .Take(pageSize)
                .Select(s => _mapper.Map<Transaction>(s))
                .ToListAsync();

            return (list, totalItems, totalPages);
        }
    }
}
