﻿using Assignment.Commons;
using System;
using System.Net;

namespace Assignment.DataAccess.Sql.Exceptions
{
    internal class SqlDatabaseMigrationException: BaseException
    {
        public SqlDatabaseMigrationException(Exception innerException)
            : base("SQL", "The sql database could not be migrated", HttpStatusCode.InternalServerError, innerException)
        {
        }
    }
}
