﻿using Assignment.Commons;
using System.Net;

namespace Assignment.DataAccess.Sql.Exceptions
{
    internal class TransactionAlreadyExistsException: BaseException
    {
        public TransactionAlreadyExistsException(int id)
            : base("Trans", $"The transaction with {id} already exists.", HttpStatusCode.BadRequest, null)
        {
        }
    }
}
