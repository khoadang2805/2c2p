﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Assignment.DataAccess.Sql.Migrations
{
    public partial class AddTransaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Transaction",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INT", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransactionId = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    Date = table.Column<DateTime>(type: "DATETIME", nullable: false, defaultValueSql: "getdate()"),
                    Amount = table.Column<decimal>(type: "DECIMAL(18,4)", nullable: false),
                    CurrencyCode = table.Column<string>(type: "VARCHAR(3)", nullable: false),
                    Status = table.Column<short>(type: "SMALLINT", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "DATETIME", nullable: false, defaultValueSql: "getdate()"),
                    CreatedBy = table.Column<string>(type: "VARCHAR(50)", nullable: false, defaultValue: "SYSTEM/UNKNOWN"),
                    ModifiedAt = table.Column<DateTime>(type: "DATETIME", nullable: false, defaultValueSql: "getdate()"),
                    ModifiedBy = table.Column<string>(type: "VARCHAR(50)", nullable: false, defaultValue: "SYSTEM/UNKNOWN")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transaction", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_CurrencyCode",
                table: "Transaction",
                column: "CurrencyCode");

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_Date",
                table: "Transaction",
                column: "Date");

            migrationBuilder.CreateIndex(
                name: "IX_Transaction_Status",
                table: "Transaction",
                column: "Status");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Transaction");
        }
    }
}
