﻿using Assignment.Commons.Constants;
using AutoMapper;

namespace Assignment.DataAccess.Sql.Mapper
{
    internal class PaymentStatusConverter : IValueConverter<TransactionStatus, PaymentStatus?>
    {
        public PaymentStatus? Convert(TransactionStatus sourceMember, ResolutionContext context)
        {
            return sourceMember switch
            {
                TransactionStatus.Approved => PaymentStatus.A,
                TransactionStatus.Done => PaymentStatus.D,
                TransactionStatus.Rejected => PaymentStatus.R,
                _ => default,
            };
        }
    }
}
