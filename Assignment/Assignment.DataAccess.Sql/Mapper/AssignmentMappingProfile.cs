﻿using Assignment.Commons.Constants;
using Assignment.Models;
using AutoMapper;
using System;
using System.Globalization;

namespace Assignment.DataAccess.Sql.Mapper
{
    internal class AssignmentMappingProfile : Profile
    {
        public AssignmentMappingProfile()
        {
            CreateMap<CreateTransaction, Entities.Transaction>()
                .ForMember(m => m.TransactionId, opt => opt.MapFrom(src => src.TransactionId))
                .ForMember(m => m.Date, opt => opt.ConvertUsing(new DateTimeConverter(), src => src.Date))
                .ForMember(m => m.Amount, opt => opt.MapFrom(src => decimal.Parse(src.Amount)))
                .ForMember(m => m.CurrencyCode, opt => opt.MapFrom(src => src.CurrencyCode))
                .ForMember(m => m.Status, opt => opt.MapFrom(src => Enum.Parse(typeof(TransactionStatus), src.Status)))
                .ForMember(m => m.Id, opt => opt.Ignore());

            CreateMap<Entities.Transaction, Transaction>()
                .ForMember(m => m.Id, opt => opt.MapFrom(src => src.TransactionId))
                .ForMember(m => m.Payment, opt => opt.MapFrom(src => $"{src.Amount:N2} {src.CurrencyCode}"))
                .ForMember(m => m.Status, opt => opt.ConvertUsing(new PaymentStatusConverter(), src => src.Status));
        }
    }
}
