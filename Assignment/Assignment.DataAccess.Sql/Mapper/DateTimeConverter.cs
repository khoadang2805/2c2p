﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.DataAccess.Sql.Mapper
{
    internal class DateTimeConverter : IValueConverter<string, DateTime>
    {
        public DateTime Convert(string sourceMember, ResolutionContext context)
        {
            if (DateTime.TryParseExact(sourceMember, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out DateTime dateVal))
            {
                return dateVal;
            }
            else
            {
                return dateVal;
            }
        }
    }
}
