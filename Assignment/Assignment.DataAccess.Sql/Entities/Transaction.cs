﻿using Assignment.Commons.Constants;
using Microsoft.EntityFrameworkCore;
using System;

namespace Assignment.DataAccess.Sql.Entities
{
    public class Transaction: DbAuditable
    {
        public int Id { get; set; }
        public string TransactionId { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public TransactionStatus Status { get; set; }

        public static void ConfigureEntity(ModelBuilder modelBuilder)
        {
            // Declaration
            var transaction = modelBuilder.Entity<Transaction>();
            transaction.ToTable("Transaction");
            // Properties
            transaction.Property("Id").HasColumnName("Id").HasColumnType("INT").UseIdentityColumn();
            transaction.Property("TransactionId").HasColumnName("TransactionId").HasColumnType("VARCHAR(50)").IsRequired(true);
            transaction.Property("Date").HasColumnName("Date").HasColumnType("DATETIME").HasDefaultValueSql("getdate()").IsRequired(true);
            transaction.Property("Amount").HasColumnName("Amount").HasColumnType("DECIMAL(18,4)").IsRequired(true);
            transaction.Property("CurrencyCode").HasColumnName("CurrencyCode").HasColumnType("VARCHAR(3)").IsRequired(true);
            transaction.Property("Status").HasColumnName("Status").HasColumnType("SMALLINT").IsRequired(true);
            // Misc
            transaction.HasKey("Id");
            transaction.HasIndex("Date");
            transaction.HasIndex("CurrencyCode");
            transaction.HasIndex("Status");
            // Auditable
            ConfigureAuditable(transaction);
        }
    }
}
