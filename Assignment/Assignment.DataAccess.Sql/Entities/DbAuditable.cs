﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Assignment.DataAccess.Sql.Entities
{
    public abstract class DbAuditable
    {
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedAt { get; set; }
        public string ModifiedBy { get; set; }

        public static void ConfigureAuditable(EntityTypeBuilder entity)
        {
            entity.Property("CreatedAt").HasColumnName("CreatedAt").HasColumnType("DATETIME").HasDefaultValueSql("getdate()").IsRequired(true);
            entity.Property("CreatedBy").HasColumnName("CreatedBy").HasColumnType("VARCHAR(50)").HasDefaultValue("SYSTEM/UNKNOWN").IsRequired(true);
            entity.Property("ModifiedAt").HasColumnName("ModifiedAt").HasColumnType("DATETIME").HasDefaultValueSql("getdate()").IsRequired(true);
            entity.Property("ModifiedBy").HasColumnName("ModifiedBy").HasColumnType("VARCHAR(50)").HasDefaultValue("SYSTEM/UNKNOWN").IsRequired(true);
        }
    }
}
