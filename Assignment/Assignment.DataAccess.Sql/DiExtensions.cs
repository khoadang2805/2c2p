﻿using Assignment.DataAccess.Sql.Mapper;
using Assignment.DataAccess.Sql.Repositories;
using Assignment.DataAccess.Sql.SqlDb;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Assignment.DataAccess.Sql
{
    public static class DiExtensions
    {
        public static IServiceCollection AddDataAccessForSql(this IServiceCollection services, string connectionString)
        {
            services.Configure<SqlOptions>(o => o.ConnectionString = connectionString);
            services.AddHostedService<DatabaseProvisioningService>();

            services.AddDbContext<DatabaseContext>(options =>
                options.UseSqlServer(connectionString, x => x.MigrationsAssembly("Assignment.DataAccess.Sql")));

            services.AddAutoMapper(typeof(AssignmentMappingProfile));
            services.AddScoped<ITransactionRepository, TransactionRepository>();

            return services;
        }
    }
}
