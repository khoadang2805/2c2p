﻿namespace Assignment.Commons.Constants
{
    public enum TransactionStatus
    {
        Approved,
        Rejected,
        Done
    }

    public enum PaymentStatus
    {
        A,
        R,
        D
    }
}
