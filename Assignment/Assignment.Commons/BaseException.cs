﻿using System.Net;

namespace Assignment.Commons
{
    public abstract class BaseException : System.Exception
    {
        public string ErrorCode { get; set; }

        public HttpStatusCode StatusCode { get; }

        protected BaseException(string errorCode, string message, HttpStatusCode statusCode, System.Exception inner = null) : base(message, inner)
        {
            ErrorCode = errorCode;
            StatusCode = statusCode;
        }

        protected BaseException(string message, System.Exception inner = null) : base(message, inner)
        {
        }
    }
}
