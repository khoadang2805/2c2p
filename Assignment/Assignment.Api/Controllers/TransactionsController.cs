﻿using Assignment.Bussiness.Managers;
using Assignment.Commons;
using Assignment.Commons.Constants;
using Assignment.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Assignment.Api.Controllers
{
    [Route("transactions")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        [HttpPost]
        [Route("file")]
        public Task UploadTransactionFile(
            [FromServices] ITransactionManager transactionManager,
            [FromForm] ImportTransaction request)
            => transactionManager.ImportTransactions(request);

        [HttpGet]
        [Route("currency")]
        public Task<PageResult<Transaction>> GetTransactionByCurrency(
            [FromServices] ITransactionManager transactionManager,
            [FromQuery] string currency,
            [FromQuery] int pageNumber = 1,
            [FromQuery] int pageSize = 10)
            => transactionManager.GetTransactionsByCurrency(currency, pageNumber, pageSize);

        [HttpGet]
        [Route("status")]
        public Task<PageResult<Transaction>> GetTransactionByStatus(
            [FromServices] ITransactionManager transactionManager,
            [FromQuery] TransactionStatus? status,
            [FromQuery] int pageNumber = 1,
            [FromQuery] int pageSize = 10)
            => transactionManager.GetTransactionsByStatus(status, pageNumber, pageSize);

        [HttpGet]
        [Route("date")]
        public Task<PageResult<Transaction>> GetTransactionByDate(
            [FromServices] ITransactionManager transactionManager,
            [FromQuery] DateTime? from,
            [FromQuery] DateTime? to,
            [FromQuery] int pageNumber = 1,
            [FromQuery] int pageSize = 10)
            => transactionManager.GetTransactionsByDate(from, to, pageNumber, pageSize);
    }
}
