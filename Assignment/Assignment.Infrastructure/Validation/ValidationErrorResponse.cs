﻿using Assignment.Commons.Exception;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace AppsensPro.Infrastructure.Validation
{
    public class ValidationErrorResponse : ExceptionResponseModel
    {
        public ModelStateDictionary ValidationResult { get; set; }
    }
}
