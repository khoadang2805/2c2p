﻿using Assignment.Commons.Constants;
using System;

namespace Assignment.Models
{
    public class CreateTransaction
    {
        public string TransactionId { get; set; }

        public string Date { get; set; }

        public string Amount { get; set; }

        public string Status { get; set; }

        public string CurrencyCode { get; set; }
    }
}
