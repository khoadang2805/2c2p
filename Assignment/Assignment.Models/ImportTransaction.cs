﻿using Microsoft.AspNetCore.Http;

namespace Assignment.Models
{
    public class ImportTransaction
    {
        public IFormFile File { get; set; }
    }
}
