﻿using Assignment.Commons;
using System.Net;

namespace Assignment.Models.Exceptions
{
    public class TransactionValidationException: BaseException
    {
        public TransactionValidationException(string message)
            : base("InvalidFile", message, HttpStatusCode.BadRequest)
        {
        }
    }
}
