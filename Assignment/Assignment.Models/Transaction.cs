﻿using Assignment.Commons.Constants;

namespace Assignment.Models
{
    public class Transaction
    {
        public string Id { get; set; }
        public string Payment { get; set; }
        public PaymentStatus? Status { get; set; }
    }
}
